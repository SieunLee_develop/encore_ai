package servlet.ajax;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/AddressServlet")
public class AddressServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HashMap<String, String> map;

	// 1. 클라이언트 요청전
	public AddressServlet() {
		map = new HashMap<String, String>();
	}

	// 2. 클라이언트 요청전
	// 디비를 연결하는 대신에 Map을 생성, 각 선수들의 주소를 매핑시킨다
	@Override
	public void init() throws ServletException {
		map.put("손흥민", "신사동");
		map.put("박지성", "압구정동");
		map.put("메시", "스페인");
		map.put("이강인", "대치동");
		map.put("안정환", "서초동");
		map.put("황희찬", "강남");

	}

	// 3.
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

	// JQuery에서 비동기로 요청이 들어왔을 때
	protected void doProcess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 로직은 여기서
		/*
		 * 1. 폼값 받아오기 .. 메시 혹은 안정환 등등 (2. dao 받아와서 비지니스 로직에 이름넣고 이름에 해당하는 사람이 사는 곳을
		 * 리턴받는다)
		 * 
		 */
		// 한글처리
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		
		PrintWriter out = response.getWriter();




		String player = request.getParameter("player");
		String address = map.get(player);

		// System.out.println(map.get("손흥민"));

		out.print(address);
	}

}
